# scaffolding for settings of vscode extensions

## Example

Define your extension settings, with default values:

```typescript
import settings from '@mkhl/vscode-settings'

export default settings('section', {
	executablePath: 'worple',
	executableArgs: [] as string[],
	ignoreErrors: false,
})
```

then import these settings (as `config`, say),
resolve their values for your scope
(a workspace folder if you have one, the global scope otherwise),
and access them:

```typescript
config.resolve(workspaceFolder).executablePath // if you have one
config.resolve().executablePath // otherwise
```

or open them in the VSCode Settings UI:

```typescript
await config.open('ignoreErrors')
```


## Supported Variables

* `${userHome}` - the path of the user's home folder
* `${cwd}` - the task runner's current working directory upon the startup of VS Code
* `${workspaceFolder}` - the path of the folder opened in VS Code (if you passed one)
* `${workspaceFolder:Name}` - the path of the folder named `Name` in VS Code
* `${env:VAR}` - the value of the environment variable `VAR`
