import os from 'os'
import process from 'process'

import vscode from 'vscode'

export class Settings<Type extends object> {
	constructor(
		public readonly section: string,
		private readonly defaults: Type,
	) {}

	public resolve(scope?: vscode.WorkspaceFolder): Type {
		const configuration = vscode.workspace.getConfiguration(this.section, scope)
		const substitutions = this.createSubstitutions(scope)
		return Object.fromEntries(
			Object.entries(this.defaults).map(([key, defaultValue]) => {
				const value = configuration.get(key)
				if (value === undefined) return [key, defaultValue]
				if (typeof value !== 'string') return [key, value]
				return [key, this.replace(substitutions, value)]
			}),
		) as Type
	}

	private createSubstitutions(
		workspaceFolder?: vscode.WorkspaceFolder,
	): Map<string, string> {
		const substitutions = new Map<string, string>()
		substitutions.set('userHome', os.homedir())
		substitutions.set('cwd', process.cwd())
		if (workspaceFolder) {
			substitutions.set('workspaceFolder', workspaceFolder.uri.fsPath)
		}
		const workspaceFolders = vscode.workspace.workspaceFolders ?? []
		for (const workspace of workspaceFolders) {
			substitutions.set(`workspaceFolder:${workspace.name}`, workspace.uri.fsPath)
		}
		for (const [key, val] of Object.entries(process.env)) {
			if (val) {
				substitutions.set(`env:${key}`, val)
			}
		}
		return substitutions
	}

	private replace(subs: Map<string, string>, str: string): string {
		for (const [key, val] of subs) {
			str = str.replace(`\${${key}}`, val)
		}
		return str
	}

	public async open(key: string & keyof Type) {
		return await vscode.commands.executeCommand(
			'workbench.action.openSettings',
			`${this.section}.${key}`,
		)
	}
}

export default function settings<Type extends object>(
	section: string,
	defaults: Type,
): Settings<Type> {
	return new Settings(section, defaults)
}
